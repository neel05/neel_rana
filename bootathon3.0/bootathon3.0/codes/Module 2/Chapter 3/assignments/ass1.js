function check() {
    var x1 = document.getElementById("t1");
    var y1 = document.getElementById("t2");
    var x2 = document.getElementById("t3");
    var y2 = document.getElementById("t4");
    var x3 = document.getElementById("t5");
    var y3 = document.getElementById("t6");
    var x = document.getElementById("t7");
    var y = document.getElementById("t8");
    var p1 = document.getElementById("p1");
    //convert the value in the number from the string
    var a1 = parseFloat(x1.value);
    var b1 = parseFloat(y1.value);
    var a2 = parseFloat(x2.value);
    var b2 = parseFloat(y2.value);
    var a3 = parseFloat(x3.value);
    var b3 = parseFloat(y3.value);
    var a = parseFloat(x.value);
    var b = parseFloat(y.value);
    //it will check the given value is number or not
    if (isNaN(a1) || isNaN(a2) || isNaN(a3) || isNaN(a) || isNaN(b1) || isNaN(b2) || isNaN(b3) || isNaN(b)) {
        alert("Enter only Numeric value :");
    }
    var a_abc = Math.abs(((a1 * (b2 - b3)) + (a2 * (b3 - b1)) + (a3 * (b1 - b2))) / 2); //calculate the are of abc
    var a_pab = Math.abs(((a1 * (b2 - b)) + (a2 * (b - b1)) + (a * (b1 - b2))) / 2); //calculate the are of pab
    var a_pbc = Math.abs(((a * (b2 - b3)) + (a2 * (b3 - b)) + (a3 * (b - b2))) / 2); //calculate the are of pbc
    var a_pca = Math.abs(((a1 * (b - b3)) + (a * (b3 - b1)) + (a3 * (b1 - b))) / 2); //calculate the are of pca
    var sum = a_pab + a_pbc + a_pca; // sum of the pab ,pbc ,pca
    if (Math.abs(a_abc - sum) < 0.0000001) //check abc-sum is < 0.000001 if it is then point is inside the triangle
     {
        p1.innerHTML = "The Point(x,y) lies Inside The Traingle";
    }
    else {
        p1.innerHTML = "The Point(x,y) lies Outside The Triangle";
    }
}
//# sourceMappingURL=ass1.js.map