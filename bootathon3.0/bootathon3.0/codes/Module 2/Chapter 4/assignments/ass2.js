function generate() {
    var input = document.getElementById("input");
    var table = document.getElementById("table_1");
    var num = parseFloat(input.value);
    //it will check the given value is number or not 
    if (isNaN(num)) {
        alert("Enter only Numeric value:");
    }
    var i = 1;
    // if table is already exists then it will delete the table 
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (i = 1; i <= num; i++) {
        var row = table.insertRow(); // inserting row
        var cell = row.insertCell(); // inserting the cell 
        var text = document.createElement("input"); // insert the input tag
        text.type = "text"; //input tag type = text
        text.style.textAlign = "center"; //text-align = center 
        text.style.width = "80px"; //width = 80px
        text.value = num.toString(); //convert the value in string
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.width = "80px";
        text.value = "*";
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.width = "80px";
        text.value = i.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.width = "80px";
        text.value = "=";
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.width = "80px";
        text.value = (num * i).toString();
        cell.appendChild(text);
    }
}
//# sourceMappingURL=ass2.js.map