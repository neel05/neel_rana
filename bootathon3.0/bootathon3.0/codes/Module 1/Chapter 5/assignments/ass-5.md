

**Theory**

**1] CREATE TABLE**

1.1 Introduction
<br>

The CREATE TABLE statement is used to create a table in SQL. A table comprises of rows and columns. So while creating tables we need to provide all the information to SQL about the names of the columns, type of data to be stored in columns, size of the data, constraints if any, etc.

<br>

i] Syntax:

<br>

create table <table_name><br>

( <column_name> <data_type> (<size>) [primary key] [not null],<br>

<column_name> <data_type> (<size>) [primary key] [not null],<br>

<column_name> <data_type> (<size>) [primary key] [not null],<br>

.<br>

.<br>

<column_name> <data_type> (<size>) [primary key] [not null],<br>

[primary key (<column_name>[,<column_name>,<column_name>..,<column_name>]<br>

);<br>

ii] Description:

|table_name|: name of the table.|
|----|----|
|column1 |: name of the first column.|
|data_type |: Type of data we want to store in the particular column.|
|size |: Size of the data we can store in a particular column.|

<br>

For example if for a column we specify the data_type as number and size as 10 then this column can store number of maximum 10 digits.

<br>

iii] Example

<br>

If you want to create a table called Employee which has all the <br>
attributes like Empid, Ename, Address, Salary.<br>

Then, issue the following command in the editor<br>

Create table Employee (Empid varchar(6) , Ename varchar(15) , Address varchar(25) , Salary number(5));

<br>

**2] ALTER TABLE**

The SQL ALTER TABLE command is used to add, delete or modify columns in an existing table. You can also use the ALTER TABLE command to add and drop various constraints on an existing table.

<br>

i] Syntax:

**a] Add a New Column**

The basic syntax of an ALTER TABLE command to add a New Column in an existing table is as follows.

alter table <table_name>
add ( <new_column_name> <new_data_type> (<new_size>) ,
<new_column_name> <new_data_type> (<new_size>) ,

.

.

<new_column_name> <new_data_type> (<new_size>) );

<br>

**b] Modify column**

It is used to modify the existing columns definition in a table. Multiple columns definitions can also be modified at once.The basic syntax of an ALTER TABLE command to change the DATA TYPE and SIZE of a column in a table is as follows: 

<br>

alter table <table_name><br>
<modify> <column_name> <new_data_type> (<new_size>),<br>
<column_name> <new_data_type> (<new_size>),<br>
.<br>
.<br>
<column_name> <new_data_type> (<new_size>); <br>

<u>ADD PRIMARY KEY</u>

The basic syntax of an ALTER TABLE command to ADD PRIMARY KEY constraint to a table is as follows.<br>

alter table <table_name><br>
add primary key<br>
(<column_name>[,<column_name>,<column_name>..,<column_name>]); <br>

<u>DROP PRIMARY KEY</u>

alter table <table_name><br>
drop primary key;<br>
                        
**c] Drop column**

DROP COLUMN is used to drop column in a table. The user can use this command to delete the unwanted columns from the table.The basic syntax of an ALTER TABLE command to DROP COLUMN in an existing table is as follows. 
                                        
alter table <table_name><br>
drop column<br>
< column_name > [,<column_name>,<column_name>..,<column_name >];

<br>

**3] DROP TABLE**

<br>

The SQL DROP TABLE statement is used to remove a table definition and all the data and constraints for that table. Once a table is deleted then all the information available in that table will also be lost forever.

<br>

**i] Syntax:**

<br>

drop table <table_name >;

<br>

ii] Example:

<br>

Let us first verify the Employee table and then we will delete it from the database as shown belo
SQL> DESC Employee;



<img src="img1.jpg">

<br>

This means that the Employee table is available in the database, so let us now drop it as shown below.<br>

SQL> DROP table Employee;<br>


<img src="img2.png">

<br>

Now, if you would try the DESC command, then you will get the following error

SQL> DESC Employee;

<br>

**"table doesn't exist : Employee"**

<br>

Overview of Data Types<br>

A data type is a classification of a particular type of information or data. Each value manipulated by database has a data type.<br>

The data type of value associates a fixed set of properties with the value.<br>

These properties allow the database to treat values of one data type differently from values of another. <br>

Data Types Supported by the Database<br>

Character_ data types<br>
{<br>
VARCHAR (size [ BYTE | CHAR ])<br>
}<br>
Number_ data types<br>
{<br>
NUMBER [ (precision [, scale ]) ]<br>
}<br>
Date_datatypes<br>
{<br>
DATE(_)<br>
}<br>


**Data Types**

<br>

|Data Type |	Description|
|----|----|
|VARCHAR(size [BYTE / CHAR]) |Variable-length character string having maximum length size bytes or characters|
| |You must specify size for VARCHAR.|
||BYTE indicates that the column will have byte length semantics|
||CHAR indicates that the column will have character semantics.|
|NUMBER |Number is used for store the numerical values in database|
|DATE |This data type contains the date time fields YEAR, MONTH, DAY, HOUR, MINUTE and SECOND. It does not have fractional seconds or a time zone |
	









	






